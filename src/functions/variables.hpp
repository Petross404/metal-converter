// If you need variables.hpp to update, alter the *.in file so cmake will configure it again.
#ifndef VARIABLES_HPP
#define VARIABLES_HPP

#define MY_PROJECT_NAME
#define MY_PROJECT_DESC

#define MY_VERSION_MAJOR
#define MY_VERSION_MINOR
#define MY_VERSION_PATCH
#define MY_VERSION_TWEAK
#define MY_VERSION ""

#endif
