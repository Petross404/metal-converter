#ifndef ENUM_STRINGS_H
#define ENUM_STRINGS_H

#include <algorithm>
#include <string>

#define ENUM_VALS( name )    name,
#define ENUM_STRINGS( name ) #name,

/** Template function to return the enum value for a given
 * string Note: assumes enums are all upper or all
 * lowercase, that they are contiguous/default-ordered, and
 * that the first value is the default
 *  @tparam ENUM     type of the enum to retrieve
 *  @tparam ENUMSIZE number of elements in the enum
 * (implicit; need not be passed in)
 *  @param valStr   string version of enum value to convert;
 * may be any capitalization (capitalization may be
 * modified)
 *  @param enumStrs array of strings corresponding to enum
 * values, assumed to all be in lower/upper case depending
 * upon enumsUpper
 *  @param enumsUpper true if the enum values are in all
 * uppercase, false if in all lowercase (mixed case not
 * supported)
 *  @return enum value corresponding to valStr, or the first
 * enum value if not found
 */
template<typename ENUM, size_t ENUMSIZE>
static inline ENUM fromString( std::string &valStr,
			       const char *( &enumStrs )[ENUMSIZE],
			       bool enumsUpper = true )
{
	ENUM e = static_cast<ENUM>( 0 );    // by default, first value
	// convert valStr to lower/upper-case
	std::transform( valStr.begin(), valStr.end(), valStr.begin(), enumsUpper ? ::toupper : ::tolower );
	for ( size_t i = 0; i < ENUMSIZE; i++ )
	{
		if ( valStr == std::string( enumStrs[i] ) )
		{
			e = static_cast<ENUM>( i );
			break;
		}
	}
	return e;
}

////////////////////
//! Define ColorType enum with array for converting to/from
//! strings
#define SHAPES( ENUM )    \
	ENUM( B_PIPE )    \
	ENUM( B_T_BAR )   \
	ENUM( B_HEX )     \
	ENUM( B_ROUND )   \
	ENUM( B_ANGLE )   \
	ENUM( B_FLAT )    \
	ENUM( B_CHAN )    \
	ENUM( B_SQU_BAR ) \
	ENUM( B_SQU_TUB ) \
	ENUM( B_BEAM )    \
	ENUM( B_SHEET )
enum SHAPE { SHAPES( ENUM_VALS ) };
static const char *shapes_type_name[] = { SHAPES( ENUM_STRINGS ) };

#endif
