#include "hex.hpp"

#include <QtCore/qglobal.h>    // for qDebug
#include <qbrush.h>	       // for QBrush
#include <qcheckbox.h>
#include <qdebug.h>		     // for QDebug
#include <qgraphicsitem.h>	     // for QGraphicsTextItem, QGraphi...
#include <qgraphicsproxywidget.h>    // for QGraphicsProxyWidget
#include <qgraphicsview.h>
#include <qgridlayout.h>    // for QGridLayout
#include <qgroupbox.h>	    // for QGroupBox
#include <qlayout.h>	    // for QLayout
#include <qlist.h>	    // for QList, QVector::toList
#include <qnamespace.h>	    // for BypassGraphicsProxyWidget
#include <qobjectdefs.h>    // for emit
#include <qpainter.h>
#include <qpen.h>	 // for QPen
#include <qpoint.h>	 // for QPointF, QPoint
#include <qpolygon.h>	 // for QPolygonF
#include <qthread.h>
#include <qtimer.h>
#include <qvector.h>	// for QVector
#include <stddef.h>	// for size_t

#include <cmath>
#include <memory>     // for make_unique, unique_ptr
#include <utility>    // for move

#include "src/dialog/shapes/shape.hpp"	  // for ProxyPtrVec, SceneInfo, Shape
#include "src/dialog/shapes/widgets/hexshape.hpp"
#include "src/dialog/widget/lineedit.h"	     // for LineEdit
#include "src/dialog/widget/shapescene.h"    // for ShapeScene
#include "src/dialog/widget/shapeview.h"
#include "src/dialog/widget/widget.h"
#include "src/functions/connectverifier.hpp"
class QObject;

/*
 * These points were calculated in this page:
 * https://www.mathopenref.com/coordpolycalc.html
 * 50, -87
 * -50, -87
 * -100, 0
 * -50, 87
 * 50, 87
 * 100, 0
 */

Hex::Hex( QObject* parent )
	: Shape{ parent }
	, m_plg{ QVector<QPointF>{ QPoint{ 50, -87 },
				   QPoint{ -50, -87 },
				   QPoint{ -100, 0 },
				   QPoint{ -50, 87 },
				   QPoint{ 50, 87 },
				   QPoint{ 100, 0 } } }
	, m_line_thickn_a{ QPoint{ 60, 90 }, QPoint{ 120, 90 } }
	, m_line_thickn_b{ QPoint{ 60, 85 }, QPoint{ 120, 85 } }
	, m_line_size_a{ QPoint{ -50, 95 }, QPointF{ -50, 125 } }
	, m_line_size_b{ QPointF{ 50, 95 }, QPointF{ 50, 125 } }
	, m_scene{ new ShapeScene{ this } }
	, m_polyItem{ std::make_unique<QGraphicsPolygonItem>( m_plg, nullptr ) }
{
	init();
	createScene();
}

void Hex::createScene()
{
	QPen hexPen{ QBrush{ Qt::GlobalColor::darkCyan }, static_cast<double_t>( m_thickness ) };

	QPen linePen{ Qt::PenStyle::DashLine };

	QGraphicsLineItem* lineThickA{ m_scene->addLine( m_line_thickn_a, linePen ) };
	QGraphicsLineItem* lineThickB{ m_scene->addLine( m_line_thickn_b, linePen ) };

	m_scene->addLine( m_line_size_a, linePen );
	m_scene->addLine( m_line_size_b, linePen );

	m_scene->addItem( m_polyItem.get() );
	m_scene->setBackgroundBrush( backgroundBrush );

	Hex::customWidget();

	/*
	 * Once something on the scene is updated, a number of things must be taken care:
	 *
	 * A) Reposition the thickness widget
	 * B) Update the text showing numeric info about sizes etc.
	 */
	ConnectVerifier v;

	v = connect( m_scene, &ShapeScene::changed, this, &Hex::changedSlot, Qt::ConnectionType::UniqueConnection );

	v = connect( m_scene,
		     &ShapeScene::resetRequested,
		     this,
		     &Hex::resetRequestedSlot,
		     Qt::ConnectionType::UniqueConnection );

	v = connect( m_widget->solidCheckBox(),
		     &QCheckBox::toggled,
		     this,
		     std::bind( &Hex::solidToggledSlot,
				this,
				std::placeholders::_1,
				m_widget->thicknessLineEdit(),
				lineThickA,
				lineThickB ),
		     Qt::UniqueConnection );

	/*
	 * Add to the scene the informative text about sizes etc.
	 */
	m_scene->addItem( m_graphTextLength.get() );
	m_scene->addItem( m_graphTextSize.get() );

	m_scene->centerItem( m_polyItem.get() );

	m_sceneInfo = std::make_unique<SceneInfo>( m_scene, m_widget, &hexPen );
}

Hex::~Hex() { delete m_widget; }

SceneInfo* Hex::sceneInfo() { return m_sceneInfo.get(); }

Widget* Hex::customWidget()
{
	m_widget = new HexWidget{ std::nullopt };

	const LineEdit* thicknessLineEdit{ m_widget->thicknessLineEdit() };
	const LineEdit* lengthLineEdit{ m_widget->lenghtLineEdit() };
	const LineEdit* sizeLineEdit{ m_widget->sizeLineEdit() };

	ConnectVerifier v;

	v = connect(
		m_widget->thicknessLineEdit(),
		&LineEdit::editingFinished,
		this,
		[this, thicknessLineEdit]() -> void {
			int oldThickness{ m_polyItem->pen().width() };
			int m_thickness{ thicknessLineEdit->text().toInt() };

			if ( m_thickness == oldThickness ) return;

			QPen pen{ QBrush{ Qt::GlobalColor::darkCyan },
				  static_cast<qreal>( m_thickness ) };

			// Temporary smart pointer
			auto p{ std::make_unique<QGraphicsPolygonItem>( m_plg, nullptr ) };
			m_polyItem = std::move( p );
			m_polyItem->setPen( pen );
			m_scene->addItem( m_polyItem.get() );
		},
		Qt::UniqueConnection );

	v = connect(
		m_widget->lenghtLineEdit(),
		&LineEdit::editingFinished,
		this,
		[this, lengthLineEdit, sizeLineEdit]() -> void {
			m_length = lengthLineEdit->text().toInt();

			const bool enableSolidCheckBox{ sizeLineEdit->text().isEmpty()
							&& lengthLineEdit->text().isEmpty() };

			// emit solidChcBoxEnable( enableSolidCheckBox );
		},
		Qt::UniqueConnection );

	v = connect(
		m_widget->sizeLineEdit(),
		&LineEdit::editingFinished,
		this,
		[this, sizeLineEdit, lengthLineEdit]() -> void {
			m_size = sizeLineEdit->text().toInt();

			const bool enableChkBox{ sizeLineEdit->text().isEmpty()
						 && lengthLineEdit->text().isEmpty() };

			// emit solidChcBoxEnable( enableChkBox );
		},
		Qt::ConnectionType::UniqueConnection );

	return m_widget;
}

std::string Hex::name() const { return "Hex"; }

std::optional<uint> Hex::area() const
{
	uint thickness{ 0 };

	if ( !isSolid() ) { thickness = m_thickness; }

	return m_size ? std::make_optional<uint>( 3 * std::sqrt( 3 )
						  * pow( m_size - thickness, 2 ) / 2 )
		      : std::nullopt;
}

void Hex::resetScene() {}

void Hex::init()
{
	// Use a temporary variable p and then move it.

	const QString s{ m_msgGraphMaterial.arg( m_materialStr ) };

	std::unique_ptr<QGraphicsTextItem> p{ std::make_unique<QGraphicsTextItem>( s ) };
	m_graphMaterial = std::move( p );
	m_graphMaterial->setPos( QPointF{ 50, 50 } );

	p = std::make_unique<QGraphicsTextItem>( m_msgGraphTextLength.arg( m_length ) );
	m_graphTextLength = std::move( p );
	m_graphTextLength->setPos( QPointF{ 115, -15 } );

	p = std::make_unique<QGraphicsTextItem>( m_msgGraphTextSize.arg( m_sizeStr ) );
	m_graphTextSize = std::move( p );
	m_graphTextSize->setPos( QPointF{ -65, 125 } );

	p = std::make_unique<QGraphicsTextItem>( m_msgGraphTextThick.arg( m_thickness ) );
	m_graphStrThick = std::move( p );
	m_graphStrThick->setPos( QPoint{ 120, -20 } );

	p	      = std::make_unique<QGraphicsTextItem>( m_lenStr );
	m_graphStrLen = std::move( p );
	m_graphStrLen->setPos( QPointF{ 120, 150 } );

	p	       = std::make_unique<QGraphicsTextItem>( m_sizeStr );
	m_graphStrSize = std::move( p );
}

void Hex::setupConnections() const { ConnectVerifier v; }

const bool Hex::isSolid() const { return m_isSolid; }

void Hex::solidToggledSlot( const bool	       checked,
			    LineEdit*	       thicknessLineEdit,
			    QGraphicsLineItem* lineThickA,
			    QGraphicsLineItem* lineThickB )
{
	m_isSolid = checked;

	// When checked for solid, disable the thickness.
	thicknessLineEdit->setDisabled( checked );

	// Temporary smart pointer
	auto p{ std::make_unique<QGraphicsPolygonItem>( m_plg, nullptr ) };
	m_polyItem = std::move( p );

	// Initialize properties based on checkbox.
	const QPen pen{ QBrush{ Qt::GlobalColor::darkCyan },
			static_cast<double_t>( m_thickness ) };

	const QBrush brush{ checked ? QBrush{ pen.color(), Qt::BrushStyle::DiagCrossPattern }
				    : m_scene->backgroundBrush() };

	const Qt::FillRule fillRule{ checked ? Qt::FillRule::OddEvenFill
					     : Qt::FillRule::WindingFill };

	// No need for thickness when solid.
	if ( checked )
	{
		m_scene->removeItem( lineThickA );
		m_scene->removeItem( lineThickB );
		thicknessLineEdit->clear();
	}
	else
	{
		m_scene->addItem( lineThickA );
		m_scene->addItem( lineThickB );
	}

	// Apply properties
	m_polyItem->setPen( pen );
	m_polyItem->setFillRule( fillRule );
	m_polyItem->setBrush( brush );

	// Finally, update.
	m_scene->addItem( m_polyItem.get() );

	std::variant<uint, QString> v;
	if ( area().has_value() ) { v = area().value(); }
	else { v = QStringLiteral( "Error" ); }

	std::visit( []( auto arg ) { qDebug() << arg; }, v );
}

void Hex::resetRequestedSlot()
{
	QPen pen;
	pen.setWidth( 4 );
	pen.setBrush( Qt::lightGray );

	// Temporary smart pointer, release immediatelly
	auto p = std::make_unique<QGraphicsPolygonItem>( m_plg, nullptr );
	m_polyItem.reset( p.release() );
	m_polyItem->setPen( pen );
	m_scene->addItem( m_polyItem.get() );
}

void Hex::changedSlot()
{
	// Read length and edit the text
	QString lenStr = m_msgGraphTextLength.arg( m_length );
	m_graphTextLength->setPlainText( lenStr );

	// Read size of side and edit the text
	QString sizeStr = m_msgGraphTextSize.arg( m_size );
	m_graphTextSize->setPlainText( sizeStr );

	// Inform the widgets if the shape is fully defined.
	emit shapeIsReady( m_size && m_length );
}
