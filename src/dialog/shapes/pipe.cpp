#include "pipe.hpp"

#include <qgraphicsitem.h>    // for QGraphicsEllipseItem
#include <qgridlayout.h>      // for QGridLayout
#include <qgroupbox.h>	      // for QGroupBox
#include <qnamespace.h>	      // for BypassGraphicsProxyWidget
#include <qrect.h>	      // for QRect
#include <qstring.h>	      // for QString

#include <utility>    // for move

#include "qgraphicsproxywidget.h"	     // for QGraphicsProxyWidget
#include "src/dialog/shapes/shape.hpp"	     // for ProxyPtrVec, SceneInfo, Shape
#include "src/dialog/widget/lineedit.h"	     // for LineEdit
#include "src/dialog/widget/shapescene.h"    // for ShapeScene
#include "src/dialog/widget/widget.h"
class QObject;

Pipe::Pipe( QObject* parent )
    : Shape{ parent }
    , m_shapeScene{ new ShapeScene{ this } }
    , m_widget{ new Widget{ std::nullopt } }
    , m_pen{QBrush{Qt::GlobalColor::black}, 1,}
    , m_backgroundBrush{Qt::GlobalColor::lightGray}
    , m_circleItem{ std::make_unique<QGraphicsEllipseItem>( QRect{ 0, 0, 210, 210 } ) }
{
	Pipe::createScene();
}

Pipe::~Pipe() { delete m_widget; }

SceneInfo* Pipe::sceneInfo() { return new SceneInfo{ m_shapeScene, m_widget, &m_pen }; }

std::string Pipe::name() const { return "Pipe"; }

void Pipe::createScene()
{
	m_circleItem->setPen( m_pen );
	m_shapeScene->addItem( m_circleItem.get() );
	m_shapeScene->setBackgroundBrush( m_backgroundBrush );

	// https://wiki.sei.cmu.edu/confluence/display/cplusplus/OOP50-CPP.+Do+not+invoke+virtual+functions+from+constructors+or+destructors
	auto		      widget{ Pipe::customWidget() };
	QGraphicsProxyWidget* proxyWidget{ m_shapeScene->addWidget( widget ) };
	/*
	    proxyDiameter->setPos( 5, 75 );
	    proxyThickness->setPos( 105, 210 );
	    proxyLength->setPos( -70, -70 );*/
}

Widget* Pipe::customWidget()
{
	Widget* widget{ new Widget{ std::nullopt } };

	QGridLayout* grid{ new QGridLayout };
	widget->setLayout( grid );

	/*
	 * Create the diameter widget for this scene
	 */
	QGroupBox*   groupDiameter{ new QGroupBox };
	QGridLayout* gridDiameter{ new QGridLayout };
	groupDiameter->setLayout( gridDiameter );
	groupDiameter->setAttribute( Qt::WA_TranslucentBackground );

	auto msg{ tr( "Enter Diameter : " ) };
	LineEdit* diameterLineEdit{ new LineEdit{ msg, new LineEditType::Size{}, groupDiameter } };
	diameterLineEdit->setWindowFlag( Qt::BypassGraphicsProxyWidget );

	connect(
		diameterLineEdit,
		&LineEdit::editingFinished,
		this,
		[this, diameterLineEdit]() -> void {
			m_diameter = diameterLineEdit->text().toInt();
		},
		Qt::QueuedConnection );

	gridDiameter->addWidget( diameterLineEdit );

	/*
	 * Create the thickness widget for this scene
	 */
	QGroupBox*   groupThickness{ new QGroupBox };
	QGridLayout* gridThickness{ new QGridLayout };
	groupThickness->setLayout( gridThickness );
	groupThickness->setAttribute( Qt::WA_TranslucentBackground );

	msg = tr( "Enter the thickness : " );
	LineEdit* thickLineEdit{
		new LineEdit{ msg, new LineEditType::Thickness{}, groupThickness } };
	thickLineEdit->setWindowFlag( Qt::BypassGraphicsProxyWidget );

	connect(
		thickLineEdit,
		&LineEdit::editingFinished,
		this,
		[this, thickLineEdit]() -> void {
			uint oldThickness = m_circleItem->pen().width();
			m_thickness	  = thickLineEdit->text().toInt();

			if ( m_thickness == oldThickness ) return;

			m_pen.setWidth( m_thickness );

			// Temporary smart pointer; release immediatelly
			auto c = std::make_unique<QGraphicsEllipseItem>( QRect{ 0, 0, 210, 210 },
									 nullptr );
			m_circleItem.reset( c.release() );
			m_circleItem->setPen( m_pen );
			m_shapeScene->addItem( m_circleItem.get() );
		},
		Qt::UniqueConnection );

	gridThickness->addWidget( thickLineEdit );

	/*
	 * Create the length widget for this scene
	 */
	QGroupBox*   groupLenght{ new QGroupBox };
	QGridLayout* gridLenght{ new QGridLayout };
	groupLenght->setLayout( gridLenght );
	groupLenght->setAttribute( Qt::WA_TranslucentBackground );

	msg = tr( "Enter the Length :" );
	LineEdit* lenLineEdit{ new LineEdit{ msg, new LineEditType::Length{}, groupLenght } };
	lenLineEdit->setWindowFlag( Qt::BypassGraphicsProxyWidget );

	connect(
		lenLineEdit,
		&LineEdit::editingFinished,
		this,
		[this, lenLineEdit]() -> void { m_length = lenLineEdit->text().toInt(); },
		Qt::UniqueConnection );

	gridLenght->addWidget( lenLineEdit );

	grid->addWidget( groupDiameter );
	grid->addWidget( groupLenght );
	grid->addWidget( groupThickness );

	return widget;
}
