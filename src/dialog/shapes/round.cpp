#include "round.hpp"

#include "src/dialog/shapes/shape.hpp"	  // for defaultSceneInfo, SceneInfo (...
class QObject;

Round::Round() = default;

Round::Round( QObject* parent )
	: Shape{ parent }
{}

Round::~Round() = default;

SceneInfo* Round::sceneInfo() { return defaultSceneInfo(); }

std::string Round::name() const { return "Round"; }
