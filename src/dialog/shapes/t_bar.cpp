#include "t_bar.hpp"

#include <qgraphicsscene.h>    // for QGraphicsScene
#include <qnamespace.h>	       // for darkCyan

#include "src/dialog/shapes/shape.hpp"	  // for defaultSceneInfo, SceneInfo (...
class QObject;

TBar::TBar() = default;

TBar::TBar( QObject* parent )
	: Shape{ parent }
{}

TBar::~TBar() = default;

SceneInfo* TBar::sceneInfo()
{
	QGraphicsScene* s{ new QGraphicsScene{} };
	s->setBackgroundBrush( Qt::darkCyan );
	return defaultSceneInfo();
}

std::string TBar::name() const { return "TBar"; }
