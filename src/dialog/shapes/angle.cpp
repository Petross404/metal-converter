#include "angle.hpp"

#include <memory>    // for unique_ptr

#include "src/dialog/shapes/shape.hpp"	  // for defaultSceneInfo, SceneInfo (...
class QObject;

Angle::Angle( QObject* parent )
	: Shape{ parent }
{}

Angle::~Angle() = default;

SceneInfo* Angle::sceneInfo() { return defaultSceneInfo(); }

std::string Angle::name() const { return "Angle"; }
