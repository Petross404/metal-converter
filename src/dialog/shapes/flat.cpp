#include "flat.hpp"

#include <memory>    // for allocator

#include "src/dialog/shapes/shape.hpp"	  // for defaultSceneInfo, SceneInfo (...
class QObject;				  // lines 7-7

Flat::Flat( QObject* parent )
	: Shape{ parent }
{}

Flat::~Flat() = default;

SceneInfo* Flat::sceneInfo() { return defaultSceneInfo(); }

std::string Flat::name() const { return "Flat"; }
