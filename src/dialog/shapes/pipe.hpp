/*
 * <one line to give the program's name and a brief idea of
 * what it does.> Copyright (C) 2020  Πέτρος Σιλιγκούνας
 * <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef PIPE_H
#define PIPE_H

#include <qbrush.h>	// for QBrush
#include <qglobal.h>	// for uint
#include <qline.h>	// for QLineF
#include <qpen.h>	// for QPen

#include <memory>    // for unique_ptr
#include <string>    // for string
#include <vector>    // for vector

#include "shape.hpp"	       // for Shape
class QGraphicsEllipseItem;    // lines 32-32
class QGroupBox;	       // lines 33-33
class QObject;
class ShapeScene;

class Pipe: public Shape
{
	Q_DISABLE_COPY_MOVE( Pipe )

public:
	explicit Pipe( QObject* parent );

	~Pipe() override;    // = default;

	//! Override Shape::scene
	/*!
	 * Creates an appropriate QGraphicsScene for a Pipe object and returns
	 * the ownership to the caller.
	 * \return std::unique_ptr<QGraphicsScene> which represents the ownership
	 */
	[[nodiscard]] SceneInfo* sceneInfo() override;

	[[nodiscard]] Widget* customWidget() override;

	//! Override Shape::name()
	/*!
	 * \return std::string that is the name of the class
	 */
	[[nodiscard]] std::string name() const override;

protected:
	//! ...
	/*!
	 * Create a scene and construct the SceneInfo
	 */
	void createScene();

private:
	ShapeScene* m_shapeScene;
	Widget*	    m_widget;
	QPen	    m_pen;
	QBrush	    m_backgroundBrush;

	std::unique_ptr<QGraphicsEllipseItem> m_circleItem;

	QLineF m_lineDiameter;
	QLineF m_lineThickness;

	uint m_diameter{ 0 };
	uint m_thickness{ 1 };
	uint m_length{ 0 };
};

#endif
