#include "sheet.hpp"

#include "src/dialog/shapes/shape.hpp"	  // for defaultSceneInfo, SceneInfo (...
class QObject;

Sheet::Sheet( QObject* parent )
	: Shape{ parent }
{}

Sheet::~Sheet() = default;

SceneInfo* Sheet::sceneInfo() { return defaultSceneInfo(); }

std::string Sheet::name() const { return "Sheet"; }
