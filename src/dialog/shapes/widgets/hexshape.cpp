// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "hexshape.hpp"

#include <qcheckbox.h>
#include <qgridlayout.h>
#include <qgroupbox.h>

#include "../../../functions/connectverifier.hpp"
#include "../../widget/lineedit.h"
#include "../../widget/widget.h"

HexWidget::HexWidget( std::optional<QWidget*> parent )
	: Widget{ parent.value_or( nullptr ) }
	, m_grid{ new QGridLayout }
	, m_gridThickness{ new QGridLayout }
	, m_gridLength{ new QGridLayout }
	, m_gridSize{ new QGridLayout }
	, m_groupThickNess{ new QGroupBox }
	, m_groupLenght{ new QGroupBox }
	, m_groupSize{ new QGroupBox }
	, m_lineEditThick{ new LineEdit{ QStringLiteral( "Enter Thickness : " ),
					 new LineEditType::Thickness{},
					 m_groupThickNess } }
	, m_lineEditLen{ new LineEdit{
		  QStringLiteral( "Enter Length : " ), new LineEditType::Length{}, m_groupLenght } }
	, m_lineEditSize{ new LineEdit{
		  QStringLiteral( "Enter Size : " ), new LineEditType::Size{}, m_groupSize } }
	, m_solidCheckBox{ new QCheckBox{ QStringLiteral( "Solid %1?" ), m_groupThickNess } }
{
	setLayout( m_grid );

	m_solidCheckBox->setObjectName( QStringLiteral( "m_solidCheckBox" ) );
	m_solidCheckBox->setEnabled( false );

	// Setup the layouts
	m_groupThickNess->setLayout( m_gridThickness );
	m_groupLenght->setLayout( m_gridLength );
	m_groupSize->setLayout( m_gridSize );

	m_gridThickness->addWidget( m_solidCheckBox );
	m_gridThickness->addWidget( m_lineEditThick );

	m_gridSize->addWidget( m_lineEditSize );

	m_gridLength->addWidget( m_lineEditLen );

	m_grid->addWidget( m_groupLenght );
	m_grid->addWidget( m_groupSize );
	m_grid->addWidget( m_groupThickNess );

	setObjectName( QStringLiteral( "Dimensions" ) );
}

HexWidget::~HexWidget() = default;

LineEdit* HexWidget::lenghtLineEdit() { return m_lineEditLen; }

LineEdit* HexWidget::sizeLineEdit() { return m_lineEditSize; }

LineEdit* HexWidget::thicknessLineEdit() { return m_lineEditThick; }

QCheckBox* HexWidget::solidCheckBox() { return m_solidCheckBox; }
