// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef HEXSHAPE_HPP
#define HEXSHAPE_HPP

#include <qwidget.h>

#include <memory>

#include "../../widget/widget.h"

class QCheckBox;
class QGroupBox;
class QGridLayout;

class LineEdit;

/*!
 * @todo write docs
 */
class HexWidget: public Widget
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( HexWidget )

public:
	explicit HexWidget( std::optional<QWidget*> parent );

	~HexWidget() override;

	LineEdit* thicknessLineEdit();
	LineEdit* lenghtLineEdit();
	LineEdit* sizeLineEdit();

	QCheckBox* solidCheckBox();

private:
	QGridLayout* m_grid;
	QGridLayout* m_gridThickness;
	QGridLayout* m_gridLength;
	QGridLayout* m_gridSize;

	QGroupBox* m_groupThickNess;
	QGroupBox* m_groupLenght;
	QGroupBox* m_groupSize;

	LineEdit* m_lineEditThick;
	LineEdit* m_lineEditLen;
	LineEdit* m_lineEditSize;

	QCheckBox* m_solidCheckBox;
};

#endif	  // HEXSHAPE_HPP
