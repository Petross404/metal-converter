/*
 * <one line to give the program's name and a brief idea of
 * what it does.> Copyright (C) 2020  Πέτρος Σιλιγκούνας
 * <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef SHAPE_H
#define SHAPE_H

#include <qbrush.h>	    // for QBrush, QLinearGradient
#include <qcolor.h>	    // for QColor
#include <qglobal.h>	    // for uint
#include <qobject.h>	    // for QObject
#include <qobjectdefs.h>    // for Q_OBJECT, signals
#include <qstring.h>	    // for QString

#include <memory>    // for unique_ptr
#include <optional>
#include <string>    // for string
#include <vector>    // for vector

class Widget;
class QGraphicsProxyWidget;    // lines 38-38
class QGroupBox;	       // lines 35-35
class QPen;		       // lines 39-39
class ShapeScene;	       // lines 36-36

//! `SceneInfo`
/*!
 * `SceneInfo` is a structure to hold useful objects to finally
 * draw a shape on the `ConvertDialog`.
 */
class SceneInfo
{
public:
	explicit SceneInfo();
	explicit SceneInfo( SceneInfo* si );
	explicit SceneInfo( std::unique_ptr<SceneInfo> up_si );
	explicit SceneInfo( ShapeScene* _scene	= nullptr,
			    Widget*	_widget = nullptr,
			    QPen*	_pen	= nullptr );

	//! Deleted copy constructor of SceneInfo class
	explicit SceneInfo( const SceneInfo& other ) = delete;
	//! Deleted copy assignement operator of SceneInfo class
	SceneInfo& operator=( const SceneInfo& other ) = delete;
	//! Deleted move constructor of ConvertDialog class
	explicit SceneInfo( SceneInfo&& other ) noexcept = delete;
	//! Deleted move assignement operator of SceneInfo class
	SceneInfo& operator=( SceneInfo&& other ) noexcept = delete;

	~SceneInfo();

	ShapeScene* scene() const;

	Widget* widget() const;

	QPen* pen() const;

private:
	ShapeScene* p_scene{ nullptr };
	Widget*	    p_widget{ nullptr };
	QPen*	    p_pen{ nullptr };
};

//! `Shape` is class that represents a generic shape.
/*!
 * `Shape` acts as a generic interface for more specific shape types.
 */
class Shape: public QObject
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( Shape )

public:
	//! Default constructor of `Shape`
	Shape() = default;

	explicit Shape( QObject* parent );

	//! Virtual defaulted destructor of `Shape`.
	~Shape() override;

	//! Virtual function for all the children of the class to inherit
	/*!
	 * \return `SceneInfo`
	 */
	[[nodiscard]] virtual SceneInfo* sceneInfo();

	[[nodiscard]] virtual Widget* customWidget();

	//! Virtual function for all the children of the class to inherit
	/*!
	 * \return std::string that is the name of the class
	 */
	[[nodiscard]] virtual std::string name() const;

	[[nodiscard]] virtual uint shapeWidth() const;

	virtual void setShapeWidth( uint width );

	[[nodiscard]] virtual uint shapeLength() const;

	[[nodiscard]] virtual std::optional<uint> area() const;

	[[nodiscard]] virtual uint volume() const;

	virtual void setShapeLength( uint len );

	virtual void resetScene();

	//! Inform the widgets if the shape is defined.
signals:
	void shapeIsReady( bool ready );

protected:
	QLinearGradient gradient;
	QBrush		backgroundBrush{ Qt::GlobalColor::gray };
	QBrush		shapeBrush{ Qt::GlobalColor::darkCyan };

private:
	uint m_width{ 0 };
	uint m_lenght{ 0 };

	void init();
};

/*
 * Implemantation helper. Use this free function and test the class.
 */
SceneInfo* defaultSceneInfo();
#endif
