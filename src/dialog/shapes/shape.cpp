#include "shape.hpp"

#include <qgroupbox.h>	  // for QGroupBox
#include <qpen.h>	  // for QPen

#include "src/dialog/widget/shapescene.h"    // for ShapeScene
#include "src/dialog/widget/widget.h"

SceneInfo::SceneInfo()
	: p_scene{ new ShapeScene{ std::nullopt } }
{}

SceneInfo::SceneInfo( SceneInfo* si )
	: p_scene{ si->p_scene }
	, p_widget{ si->p_widget }
	, p_pen{ si->p_pen }
{}

SceneInfo::SceneInfo( ShapeScene* _scene, Widget* _widget, QPen* _pen )
	: p_scene{ _scene }
	, p_widget{ _widget }
	, p_pen{ _pen }
{}

SceneInfo::~SceneInfo()
{ /*delete p_scene;*/
}

ShapeScene* SceneInfo::scene() const { return p_scene; }

Widget* SceneInfo::widget() const { return p_widget; }

QPen* SceneInfo::pen() const { return p_pen; }

Shape::Shape( QObject* parent )
	: QObject{ parent }
{
	init();
}

Shape::~Shape() = default;

std::string Shape::name() const { return "shape"; }

SceneInfo* Shape::sceneInfo() { return defaultSceneInfo(); }

Widget* Shape::customWidget() { return new Widget{ std::nullopt }; }

uint Shape::shapeWidth() const { return m_width; }

void Shape::setShapeWidth( uint width ) { m_width = width; }

uint Shape::shapeLength() const { return m_lenght; }

void Shape::setShapeLength( uint len ) { m_lenght = len; }

std::optional<uint> Shape::area() const { return uint{}; }

uint Shape::volume() const { return uint{}; }

void Shape::resetScene() {}

// TODO When settings are applied later, this fn should initialize the scene
void Shape::init() {}

// Free function but define it here
SceneInfo* defaultSceneInfo()
{
	SceneInfo* si{ new SceneInfo{ new ShapeScene{ nullptr }, new Widget{ nullptr }, new QPen } };
	return si;
}
