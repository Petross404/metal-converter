/*
 * <one line to give the program's name and a brief idea of
 * what it does.> Copyright (C) 2020  Πέτρος Σιλιγκούνας
 * <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef FLAT_H
#define FLAT_H

#include <memory>    // for unique_ptr
#include <string>    // for string

#include "shape.hpp"	// for Shape
class QObject;

class Flat: public Shape
{
	Q_DISABLE_COPY_MOVE( Flat )

public:
	Flat() = default;
	explicit Flat( QObject* parent );

	~Flat() override;    // = default;

	//! Override Shape::scene
	/*!
	 * Creates an appropriate QGraphicsScene for a Flat object and returns
	 * the ownership to the caller.
	 * \return std::unique_ptr<QGraphicsScene> which represents the ownership
	 */
	[[nodiscard]] SceneInfo* sceneInfo() override;

	//! Override Shape::name()
	/*!
	 * \return std::string that is the name of the class
	 */
	[[nodiscard]] std::string name() const override;

private:
};
#endif
