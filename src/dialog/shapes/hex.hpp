class QGraphicsLineItem;
#ifndef HEX_H
#define HEX_H

/*
 * <one line to give the program's name and a brief idea of
 * what it does.> Copyright (C) 2020  Πέτρος Σιλιγκούνας
 * <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <qglobal.h>	 // for uint
#include <qline.h>	 // for QLineF
#include <qpolygon.h>	 // for QPolygonF
#include <qstring.h>	 // for QString

#include <memory>    // for unique_ptr
#include <string>    // for string
#include <vector>    // for vector

#include "shape.hpp"	// for Shape
class HexWidget;
class QGraphicsPolygonItem;
class QGraphicsTextItem;
class QGroupBox;    // lines 27-27
class QObject;
class ShapeScene;    // lines 29-29
class LineEdit;

class Hex: public Shape
{
	Q_DISABLE_COPY_MOVE( Hex )

public:
	Hex( QObject* parent = nullptr );

	~Hex() override;    // default;

	//! Override Shape::scene
	/*!
	 * Creates an appropriate QGraphicsScene for a Hex object and returns
	 * the ownership to the caller.
	 * \return SceneInfo which represents the ownership
	 */
	[[nodiscard]] SceneInfo* sceneInfo() override;

	/*!
	 * Initialize and return the custom widget for this shape
	 * This isn't marked as nodiscard because we don't mind not assigning the result.
	 *\return The custom widget for this shape.
	 */
	Widget* customWidget() override;

	[[nodiscard]] std::optional<uint> area() const override;

	//! Override Shape::name()
	/*!
	 * \return std::string that is the name of the class
	 */
	[[nodiscard]] std::string name() const override;

	void resetScene() override;

protected:
	/*!
	 * Create a scene and construct the SceneInfo
	 */
	void createScene();

private:
	QPolygonF m_plg;
	QLineF	  m_line_thickn_a;
	QLineF	  m_line_thickn_b;
	QLineF	  m_line_size_a;
	QLineF	  m_line_size_b;

	ShapeScene* m_scene;
	HexWidget*  m_widget;

	std::unique_ptr<SceneInfo> m_sceneInfo;

	std::unique_ptr<QGraphicsPolygonItem> m_polyItem; /*!< Polygon item aka as HEX */

	std::unique_ptr<QGraphicsTextItem> m_graphMaterial;   /*!< Display material */
	std::unique_ptr<QGraphicsTextItem> m_graphTextLength; /*!< Length of ... */
	std::unique_ptr<QGraphicsTextItem> m_graphTextSize;   /*!< Size of side.. */
	std::unique_ptr<QGraphicsTextItem> m_graphStrLen;     /*!< Display m_lenStr */
	std::unique_ptr<QGraphicsTextItem> m_graphStrSize;    /*!< Display m_sizeStr */
	std::unique_ptr<QGraphicsTextItem> m_graphStrThick;   /*!< Display m_thickness */

	const QString m_materialStr{
		QStringLiteral( "<br><em>Empty material</em>. Please select one!</br>" ).toHtmlEscaped() };
	const QString m_lenStr{ "Length" };
	const QString m_sizeStr{ "Size" };

	const QString m_msgGraphMaterial{ "The selected material is:\t %1" };
	const QString m_msgGraphTextLength{ "Length of Hex is (mm):\t %1" };
	const QString m_msgGraphTextSize{ "Size of side is: (mm)\t %1" };
	const QString m_msgGraphTextThick{ "Thickness of Hex is: (mm)\t %1" };

	// Units in mm
	uint m_length{ 0 };
	uint m_thickness{ 1 };
	uint m_size{ 0 };
	bool m_isSolid{ false };

	void init();
	void setupConnections() const;

	const bool isSolid() const;

private slots:
	void resetRequestedSlot();

	void changedSlot();

	void solidToggledSlot( const bool	  checked,
			       LineEdit*	  thicknessLineEdit,
			       QGraphicsLineItem* lineThickA,
			       QGraphicsLineItem* lineThickB );
};

#endif	  // HEX_H
