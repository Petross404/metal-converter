/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2020  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLPREVIEW_HPP
#define GLPREVIEW_HPP

#include <qobjectdefs.h>	 // for Q_OBJECT
#include <qopenglfunctions.h>	 // for QOpenGLFunctions
#include <qopenglwidget.h>	 // for QOpenGLWidget
#include <qstring.h>		 // for QString

#include <memory>
class QGraphicsView;
class QObject;

class GLView
	: public QOpenGLWidget
	, protected QOpenGLFunctions
{
	Q_OBJECT
public:
	explicit GLView( QGraphicsView* parent = nullptr );
	explicit GLView( const GLView& other )	     = delete;
	GLView& operator=( const GLView& other )     = delete;
	explicit GLView( GLView&& other ) noexcept   = delete;
	GLView& operator=( GLView&& other ) noexcept = delete;

	~GLView() override;    // = default;

protected:
	void initializeGL() override;
	void resizeGL( int w, int h ) override;
	void paintGL() override;

private:
	std::unique_ptr<QOpenGLContext> context;
};

#endif	  // GLPREVIEW_HPP
