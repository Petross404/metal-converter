/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2020  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "glview.hpp"

#include <QtGui/qopengl.h>    // for glColor3f, glVertex3f, glLoadIdentity
#include <qgraphicsview.h>    // for QGraphicsView

GLView::GLView( QGraphicsView* parent )
	: QOpenGLWidget{ parent }
	, context{ std::make_unique<QOpenGLContext>( parent ) }
{}

GLView::~GLView() = default;

void GLView::initializeGL()
{
	makeCurrent();
	initializeOpenGLFunctions();
	glClearColor( 0, 0, 0, 1 );
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_LIGHT0 );
	glEnable( GL_LIGHTING );
	glColorMaterial( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE );
	glEnable( GL_COLOR_MATERIAL );
}

void GLView::paintGL()
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glBegin( GL_TRIANGLES );
	glColor3f( 1.0, 0.0, 0.0 );
	glVertex3f( -0.5, -0.5, 0 );
	glColor3f( 0.0, 1.0, 0.0 );
	glVertex3f( 0.5, -0.5, 0 );
	glColor3f( 0.0, 0.0, 1.0 );
	glVertex3f( 0.0, 0.5, 0 );
	glEnd();
}

void GLView::resizeGL( int w, int h )
{
	glViewport( 0, 0, w, h );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	auto div = static_cast<int>( w / h );
	/*glm::mat4 projection = glm::perspective(
		45, div,
		0.01, 100.0);*/

	// gluPerspective(45, (float)w/h, 0.01, 100.0);
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	// auto lookAt = glm::lookAt( 0, 0, 5, 0, 0, 0, 0, 1, 0);
	// gluLookAt(0, 0, 5, 0, 0, 0, 0, 1, 0);
}
