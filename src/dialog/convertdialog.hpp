/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2020  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIALOG_H
#define DIALOG_H
#include <qobjectdefs.h>    // for Q_OBJECT, signals
#include <qstring.h>	    // for QString
#include <qwidget.h>	    // for QWidget

#include <memory>

class PushButton;    // lines 29-29
class QObject;	     // lines 30-30
class ShapeView;     // lines 31-31
namespace Ui {
class ConvertDialog;
}    // namespace Ui

/*!
 * `ConvertDialog` inherits from `QWidget` and is the window in which the program
 * shows the graphics for the shapes and accepts user input about the convertions.
 */
class ConvertDialog: public QWidget
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( ConvertDialog )

public:
	/*!
	 * Constructor of `ConvertDialog` class
	 * \param pbtn is a pointer to `PushButton` that triggered this construction.
	 * \param parent is a pointer to the `QWidget` that will become parent.
	 * \param title is the title of the dialog. It is defaulted to "Dialog" but
	 * it will be given a shape's name \sa Shape::name()
	 */
	explicit ConvertDialog( PushButton*    pbtn,
				QWidget*       parent,
				const QString& title = tr( "Dialog" ) );

	~ConvertDialog() override;

signals:
	/*!
	 * This signal informs QMdiSubWindow that it should close the
	 * current window. Without this signal, the ConvertDialog
	 * widget will indeed close, but the QMdiSubWindow containing
	 * it will remain visible.
	 */
	void aboutToCloseDlg();

private:
	std::unique_ptr<Ui::ConvertDialog> ui; /*!< The UI object of the class*/

	ShapeView* m_graphsview; /*!< */

	/*!
	 * Every call to `connect` that has to be made for this class
	 * is put in here.
	 * \param pbtn is the `PushButton` that will emit signals.
	 */
	void setupConnect( PushButton* pbtn );

	void createToolBox( PushButton* pbtn );
};

#endif
