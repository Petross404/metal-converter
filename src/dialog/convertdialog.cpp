/*
 * <one line to give the program's name and a brief idea of
 * what it does.> Copyright (C) 2020  Πέτρος Σιλιγκούνας
 * <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "convertdialog.hpp"

#include <qgridlayout.h>    // for QGridLayout
#include <qmessagebox.h>    // for QMessageBox, QMessageBox...
#include <qnamespace.h>	    // for UniqueConnection, AlignC...
#include <qpainter.h>	    // for QPainter, QPainter::Anti...
#include <qpushbutton.h>    // for QPushButton
#include <qsizepolicy.h>    // for QSizePolicy, QSizePolicy...

#include "../PushButton/pushbutton.hpp"	       // for PushButton
#include "../dialog/widget/shapeview.h"	       // for ShapeView
#include "../functions/connectverifier.hpp"    // for ConnectVerifier
#include "materials/materials.hpp"	       // for Materials
#include "shapes/shape.hpp"		       // for Shape, SceneInfo
#include "src/dialog/widget/materialwidget.h"
#include "src/dialog/widget/toolbox.h"
#include "src/dialog/widget/widget.h"
#include "ui_convertdialog.h"	 // for ConvertDialog

//! Alias for `QMessageBox::StandardButton`
using Button = QMessageBox::StandardButton;

static const Materials materials;

ConvertDialog::ConvertDialog( PushButton* pbtn, QWidget* parent, const QString& title )
	: QWidget{ parent, Qt::Window }
	, ui{ std::make_unique<Ui::ConvertDialog>() }
	, m_graphsview{ new ShapeView( pbtn, this ) }
{
	ui->setupUi( this );

	setWindowTitle( title );
	ui->btnCalculate->setEnabled( false );

	ui->gridGL->addWidget( m_graphsview, 0, 0 );
	m_graphsview->setRenderHint( QPainter::Antialiasing );
	m_graphsview->setAlignment( Qt::AlignCenter );
	m_graphsview->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
	m_graphsview->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );    // Disable scroll vertically
	m_graphsview->setAlignment( Qt::AlignCenter );
	m_graphsview->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );

	setupConnect( pbtn );
	createToolBox( pbtn );
}

ConvertDialog::~ConvertDialog() = default;

void ConvertDialog::setupConnect( PushButton* pbtn )
{
	const Shape* shape{ pbtn->shapeType() };

	ConnectVerifier v;

	// Close this Dialog
	v = connect( ui->btnBack,
		     &QPushButton::clicked,
		     this,
		     &ConvertDialog::aboutToCloseDlg,
		     Qt::ConnectionType::UniqueConnection );

	v = connect( this,
		     &ConvertDialog::aboutToCloseDlg,
		     shape,
		     &Shape::resetScene,
		     Qt::ConnectionType::UniqueConnection );

	v = connect( pbtn->shapeType(),
		     &Shape::shapeIsReady,
		     ui->btnCalculate,
		     &QPushButton::setEnabled,
		     Qt::ConnectionType::UniqueConnection );
}

void ConvertDialog::createToolBox( PushButton* pbtn )
{
	Shape* shape{ pbtn->shapeType() };

	MaterialWidget* materialwidget{ new MaterialWidget{ this, shape } };
	ui->gridView->addWidget( materialwidget );
}
