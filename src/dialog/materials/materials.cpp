#include "materials.hpp"

#include <QtCore/qglobal.h>    // for qDebug
#include <qbytearray.h>	       // for QByteArray
#include <qdebug.h>	       // for QDebug
#include <qfile.h>	       // for QFile
#include <qiodevice.h>	       // for operator|, QIODevice, QIODevice::ReadOnly
#include <qjsonarray.h>	       // for QJsonArray
#include <qjsondocument.h>     // for QJsonDocument, QJsonParseError
#include <qjsonvalue.h>	       // for QJsonValueRef

#include <ext/alloc_traits.h>	 // for __alloc_traits<>::value_type

#include "path.hpp"

Materials::Materials()
	: m_materialDesc{ new MaterialsDescription }
{
	QString jsonName{ QString::fromUtf8( std::string{ PATH }.c_str() )
			  + QString::fromUtf8( "material.json" ) };

	QFile jsonFile{ jsonName };

	if ( jsonFile.exists() ) { qDebug() << jsonName; }

	// In Debug builds assert if qfile can't open
	Q_ASSERT_X( jsonFile.open( QIODevice::ReadOnly | QIODevice::Text ),
		    QObject::tr( "Error" ).toStdString().c_str(),
		    jsonFile.errorString().toStdString().c_str() );

	QByteArray data = jsonFile.readAll();
	jsonFile.close();

	QJsonParseError jsonError;
	QJsonDocument	doc = QJsonDocument::fromJson( data, &jsonError );
	if ( doc.isNull() )
	{
		qDebug() << "Parse failed";
		exit( -4 );
	}

	m_jsonObject	  = doc.object();
	QJsonArray jarray = m_jsonObject.value( "Materials" ).toArray();

	// Assert in Debug builds.
	Q_ASSERT_X( !jarray.empty(), "Empty JSON Array", "" );

	// Iterate the json array and register the material properties.
	for ( int i = 0; i < jarray.size(); ++i )
	{
		const QJsonObject json = jarray.at( i ).toObject();

		const QString materialName{ json.value( "name" ).toString() };
		const double  materialDens{ json.value( "density" ).toDouble() };

		m_materialDesc->registerMaterial( materialName, materialDens );
	}
}

void MaterialsDescription::registerMaterial( const QString& name, const double density )
{
	registerMaterial( std::make_pair( name, density ) );
}

void MaterialsDescription::registerMaterial( std::pair<const QString, const double> material )
{
	const QString materialName{ material.first };
	if ( !m_materialMap.size() || !m_materialMap.contains( materialName ) )
	{
		m_materialMap.insert( material );
	}
}

std::map<const QString, const double> MaterialsDescription::materialsTable() const
{
	return m_materialMap;
}

Materials::~Materials() { delete m_materialDesc; }

MaterialsDescription* Materials::description() const { return m_materialDesc; }

MaterialsDescription& MaterialsDescription::operator=( const MaterialsDescription& m )
{
	if ( this != &m ) { this->m_materialMap = m.m_materialMap; }
	return *this;
}

size_t MaterialsDescription::size() const { return m_materialMap.size(); }
