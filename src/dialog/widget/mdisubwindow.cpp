// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "mdisubwindow.h"

MdiSubWindow::MdiSubWindow( QWidget* parent, std::optional<Qt::WindowFlags> flags )
	: QMdiSubWindow{ parent, flags.value_or( Qt::WindowFlags() ) }
{
	setMinimumSize( QSize{ 800, 550 } );
}

void MdiSubWindow::closeEvent( QCloseEvent* closeEvent ) {}
