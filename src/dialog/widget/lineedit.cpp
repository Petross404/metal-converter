// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "lineedit.h"

#include <qevent.h>	   // for QContextMenuEvent, QFocusEvent (ptr only)
#include <qmenu.h>	   // for QMenu
#include <qnamespace.h>	   // for UniqueConnection, ConnectionType, Key, Key_E...
#include <qsize.h>	   // for QSize
#include <qtimer.h>	   // for QTimer

#include <utility>    // for move
class QWidget;

namespace LineEditType {

Type::Type( std::unique_ptr<Type> t ) { *this = *t.release(); }

Type::Type( Type* t ) { *this = *t; }

Length::Length( std::unique_ptr<Type> t )
	: Type{ std::move( t ) } {};

Length::Length( Type* t )
	: Type{ t }
{}

Size::Size( std::unique_ptr<Size> t )
	: Type{ std::move( t ) }
{}

Size::Size( Type* t )
	: Type{ t }
{}

Thickness::Thickness( std::unique_ptr<Thickness> t )
	: Type{ std::move( t ) } {};

Thickness::Thickness( Type* t )
	: Type{ t }
{}

Diameter::Diameter( std::unique_ptr<Diameter> t )
	: Type{ std::move( t ) } {};

Diameter::Diameter( Type* t )
	: Type{ t }
{}

};    // namespace LineEditType

constexpr int g_not_found = -1;

static const char g_empty[] = "";

LineEdit::LineEdit( const QString& text, LineEditType::Type* type, std::optional<QWidget*> parent )
	: QLineEdit{ parent.value_or( nullptr ) }
	, m_type{ type }
	, m_txt{ text }
	, normalStyleSheet{ QStringLiteral( "color: black; \
			background-color: white; \
			font: normal 12px;" ) }
	, italicStyleSheet{ QStringLiteral( "color: gray; \
			background-color: white; \
			font: italic 12px;" ) }
	, warningStyleSheet{ QStringLiteral( "color: red; \
			background-color: white; \
			font: bold 12px;" ) }
{
	setClearButtonEnabled( true );

	setStyleSheet( italicStyleSheet );
	setObjectName( QStringLiteral( "widthlineedit" ) );
	setText( m_txt );

	setMinimumSize( QSize{ 100, 20 } );

	setupConnections();
}

LineEdit::~LineEdit() = default;

void LineEdit::setupConnections()
{
	connect( this, &LineEdit::focusChanged, this, &LineEdit::hideCursor, Qt::ConnectionType::UniqueConnection );

	connect( this,
		 &LineEdit::editingFinished,
		 this,
		 &LineEdit::editingFinishedSlot,
		 Qt::ConnectionType::UniqueConnection );

	connect( this,
		 &LineEdit::textEdited,
		 this,
		 &LineEdit::textEditedSlot,
		 Qt::ConnectionType::UniqueConnection );
}

void LineEdit::setMaximumValue( int max ) { maximumValue = max; }

void LineEdit::hideCursor( bool focus ) { setReadOnly( focus ); }

void LineEdit::setLineEditStatus( const QString& txt, const QString& stylesheet, Status status )
{
	setText( txt );
	setStyleSheet( stylesheet );
	m_status = status;
}

LineEditType::Type* LineEdit::type() const { return m_type.get(); }

void LineEdit::focusInEvent( QFocusEvent* fevent )
{
	if ( text().indexOf( m_txt ) != g_not_found )
	{
		setLineEditStatus( g_empty, normalStyleSheet, Status::EDITING );
	}
	emit focusChanged( false );

	QLineEdit::focusInEvent( fevent );
}

void LineEdit::focusOutEvent( QFocusEvent* fevent )
{
	// Reset lineedit in these two cases
	if ( text().isEmpty() || !text().toInt() )
	{
		setLineEditStatus( m_txt, italicStyleSheet, Status::MSG );
	}
	emit focusChanged( true );

	QLineEdit::focusInEvent( fevent );
}

void LineEdit::keyPressEvent( QKeyEvent* event )
{
	if ( event->key() != Qt::Key::Key_Enter )
	{
		if ( text().indexOf( m_txt ) != g_not_found )
		{
			setLineEditStatus( g_empty, normalStyleSheet, Status::EDITING );
		}
	}

	QLineEdit::keyPressEvent( event );
}

void LineEdit::mousePressEvent( QMouseEvent* event )
{
	if ( event->button() == Qt::MouseButton::LeftButton )
	{
		if ( text().indexOf( m_txt ) != g_not_found )
		{
			setLineEditStatus( g_empty, normalStyleSheet, Status::EDITING );
		}
	}

	QLineEdit::mousePressEvent( event );
}

void LineEdit::contextMenuEvent( QContextMenuEvent* event )
{
	std::unique_ptr<QMenu> menu{ createStandardContextMenu() };
	menu->setStyleSheet( normalStyleSheet );
	menu->exec( event->globalPos() );

	event->accept();
}

void LineEdit::editingFinishedSlot()
{
	if ( static_cast<bool>( m_status == Status::ERROR ) )
	{
		QString msg{ tr( "Only numeric arguments are accepted!" ) };
		setLineEditStatus( msg, normalStyleSheet, Status::EDITING );

		QTimer::singleShot( 600, [this]() -> void {
			setLineEditStatus( m_txt, italicStyleSheet, Status::MSG );
		} );
	}
	else if ( bool ok; text().toInt( &ok, 10 ) )
	{
		int w = text().toInt( &ok, 10 );

		// Don't allow crazy values
		if ( w > maximumValue ) { w = maximumValue; }
	}
}

void LineEdit::textEditedSlot( const QString& currText )
{
	QString styleSheet;
	bool	ok;

	// Non-digits are unacceptable here
	if ( !currText.toInt( &ok, 10 ) )
	{
		styleSheet = warningStyleSheet;
		m_status   = Status::ERROR;
	}
	else	// Continue normally
	{
		switch ( currText.indexOf( m_txt ) )
		{
			case g_not_found:
				styleSheet = normalStyleSheet;
				m_status   = Status::EDITING;
				break;
			default:
				styleSheet = italicStyleSheet;
				m_status   = Status::MSG;
				break;
		}
	}
	setStyleSheet( styleSheet );

	if ( currText.isEmpty() )
	{
		setStyleSheet( italicStyleSheet );
		setText( m_txt );
		return;
	}
	if ( currText.indexOf( m_txt ) != g_not_found )
	{
		QString tmpStr = currText;
		tmpStr.remove( m_txt );
		setText( tmpStr );
		return;
	}
}
