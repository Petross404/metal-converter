/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2020  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIDGET_H
#define WIDGET_H

#include <qobjectdefs.h>    // for Q_OBJECT
#include <qstring.h>	    // for QString
#include <qwidget.h>	    // for QWidget

#include <optional>

class QObject;
class QPaintEvent;    // lines 24-24

class Widget: public QWidget
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( Widget )

public:
	/**
	 * Default constructor
	 */
	explicit Widget( std::optional<QWidget*> parent );

	~Widget() override;

protected:
	void paintEvent( QPaintEvent* event ) override;
};

#endif	  // WIDGET_H
