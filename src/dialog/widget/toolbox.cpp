// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "toolbox.h"

ToolBox::ToolBox( std::optional<QWidget*> parent, std::optional<Qt::WindowFlags> flags )
	: QToolBox{ parent.value_or( nullptr ), flags.value_or( Qt::WindowFlags() ) }
{}

ToolBox::~ToolBox() = default;
