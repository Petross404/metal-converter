/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2020  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "widget.h"

#include <qbrush.h>	 // for QBrush
#include <qcolor.h>	 // for QColor
#include <qglobal.h>	 // for Q_UNUSED
#include <qpalette.h>	 // for QPalette

#include "qpainter.h"	 // for QPainter

Widget::Widget( std::optional<QWidget*> parent )
	: QWidget{ parent.value_or( nullptr ) }
{}

Widget::~Widget() = default;

void Widget::paintEvent( QPaintEvent* event )
{
	Q_UNUSED( event );

	QColor backgroundColor = palette().light().color();
	backgroundColor.setAlpha( 0 );
	QPainter cPainter( this );
	cPainter.fillRect( rect(), backgroundColor );
}
