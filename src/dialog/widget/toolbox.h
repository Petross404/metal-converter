// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef TOOLBOX_H
#define TOOLBOX_H

#include <qtoolbox.h>

#include <optional>

class ToolBox: public QToolBox
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( ToolBox )

public:
	ToolBox( std::optional<QWidget*>	parent = std::nullopt,
		 std::optional<Qt::WindowFlags> flags  = std::nullopt );

	~ToolBox() override;
};

#endif	  // TOOLBOX_H
