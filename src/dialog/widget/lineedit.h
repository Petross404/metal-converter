// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef LINEEDIT_H
#define LINEEDIT_H

#include <qglobal.h>	    // for uint
#include <qlineedit.h>	    // for QLineEdit
#include <qobjectdefs.h>    // for slots, Q_OBJECT, signals
#include <qstring.h>	    // for QString
class QContextMenuEvent;    // lines 13-13
class QFocusEvent;	    // lines 10-10
class QKeyEvent;	    // lines 11-11
class QMouseEvent;	    // lines 12-12
class QObject;
class QWidget;

#include <optional>

namespace LineEditType {
class Type
{
public:
	Type() = default;
	Type( std::unique_ptr<Type> t );
	Type( Type* t );
};

class Length: public Type
{
public:
	Length() = default;
	Length( std::unique_ptr<Type> t );
	Length( Type* t );
};

class Size: public Type
{
public:
	Size() = default;
	Size( std::unique_ptr<Size> t );
	Size( Type* t );
};

class Thickness: public Type
{
public:
	Thickness() = default;
	Thickness( std::unique_ptr<Thickness> t );
	Thickness( Type* t );
};

class Diameter: public Type
{
public:
	Diameter() = default;
	Diameter( std::unique_ptr<Diameter> t );
	Diameter( Type* t );
};
};    // namespace LineEditType

//! \class Status represented the status the LineEdit is currently in
enum class Status : uint {
	ERROR = 0,    //<! ERROR represents the error state of wrong input
	MSG,	      //<! MSG represents the default state that prints the message
	EDITING	      //<! EDITING represents the waiting for input state
};

/*!
 * Override `QLineEdit` to implement custom functionality like accepting
 * only numeric values etc.
 */
class LineEdit: public QLineEdit
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( LineEdit )
public:
	/*!
	 * Constructor
	 * \param text is the text that the lineedit should have as when not focused.
	 * \param type is the type of information it should keep (`LineEdit::Type*`).
	 * \param parent is the parent widget of this lineedit.
	 */
	explicit LineEdit( const QString&	   text,
			   LineEditType::Type*	   type,
			   std::optional<QWidget*> parent );

	~LineEdit() override;

	/*!
	 * Set a maximum value.
	 * \param max is the maximum value that it accepts.
	 */
	void setMaximumValue( int max );

	[[nodiscard]] LineEditType::Type* type() const;

public slots:
	void hideCursor( bool focus );

protected:
	void focusInEvent( QFocusEvent* fevent ) override;
	void focusOutEvent( QFocusEvent* fevent ) override;
	void keyPressEvent( QKeyEvent* event ) override;
	void mousePressEvent( QMouseEvent* event ) override;
	void contextMenuEvent( QContextMenuEvent* event ) override;

protected slots:
	void editingFinishedSlot();
	void textEditedSlot( const QString& currText );

private:
	std::unique_ptr<LineEditType::Type> m_type;

	uint	maximumValue = 10;	/*!< Maximum value */
	QString m_txt;			/*!< The text that is shown */
	QString normalStyleSheet;	/*!< Stylesheet to use when in writing mode */
	QString italicStyleSheet;	/*!< Stylesheet to use when in waiting mode */
	QString warningStyleSheet;	/*!< Stylesheet to use when in warning mode */
	Status	m_status = Status::MSG; /*!< Status of the widget */

	/*!
	 * Set up any connection that has to be done.
	 */
	void setupConnections();

	/*!
	 * Set the status of the line edit
	 * \param txt is the text to show
	 * \param stylesheet is the stylesheet to use when showing the txt
	 * \param status is the status of the wigdet
	 */
	void setLineEditStatus( const QString& txt, const QString& stylesheet, Status status );

signals:
	/*!
	 * The focus of the widget changed.
	 * \param focus is either `true` or `false`
	 */
	void focusChanged( bool focus );
	/*!
	 * Inform any listener about the new value.
	 * \param value is the newly entered numeric value.
	 */
	void lineeditFinished( int value );
};

#endif	  // LINEEDIT_H
