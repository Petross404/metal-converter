// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef GRAPHICSSCENE_H
#define GRAPHICSSCENE_H

#include <QtConcurrent/qtconcurrentrun.h>
#include <qfuturewatcher.h>
#include <qgraphicsscene.h>    // for QGraphicsScene
#include <qobjectdefs.h>       // for Q_OBJECT, signals, slots
#include <qstring.h>	       // for QString

#include <optional>

class QGraphicsItem;
class QObject;

//! ShapeScene is a subclassed `QGraphicsScene`
/*!
 * `ShapeScene` inherits `QGraphicsScene` and implements some
 * needed functionality. More specifically it defines some functions
 * and signals that various listeners need to connect and be notified
 * about.
 * \sa `ShapeScene::itemIsUpdated(QGraphicsItem* item)`
 */
class ShapeScene: public QGraphicsScene
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( ShapeScene )

public:
	//! Ctor for `ShapeScene`
	explicit ShapeScene( std::optional<QObject*> parent );
	~ShapeScene() override;

	//! Make sure that `item` is always visible
	/*!
	 * \sa `QGraphicsView::ensureVisible(const QGraphicsItem* item)`
	 */
	void centerItem( QGraphicsItem* item );

public slots:
	void resetSceneToDefault();

signals:
	//! Signal that `item` is updated somehow (maybe it's color, size, pen whatever).
	void itemIsUpdated( QGraphicsItem* item );
	void resetRequested();

	void sceneUpdateStarted() const;
	void sceneUpdateFinished() const;
};

#endif	  // GRAPHICSSCENE_H
