// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "shapescene.h"

#include <qapplication.h>
#include <qgraphicsitem.h>
#include <qgraphicsview.h>    // for QGraphicsView
#include <qlist.h>	      // for QList, QList<>::iterator
#include <qthread.h>
#include <qtimer.h>

class QGraphicsItem;
class QObject;

ShapeScene::ShapeScene( std::optional<QObject*> parent )
	: QGraphicsScene{ parent.value_or( nullptr ) }
{}

ShapeScene::~ShapeScene() = default;

void ShapeScene::resetSceneToDefault() { emit resetRequested(); }

void ShapeScene::centerItem( QGraphicsItem* item )
{
	for ( QGraphicsView* view : views() ) { view->ensureVisible( item ); }
}
