// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SUBWINDOW_H
#define SUBWINDOW_H

#include <qmdisubwindow.h>

#include <optional>

class MdiSubWindow: public QMdiSubWindow
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( MdiSubWindow )

public:
	MdiSubWindow( QWidget* parent, std::optional<Qt::WindowFlags> flags );

protected:
	void closeEvent( QCloseEvent* closeEvent ) override;
};

#endif	  // SUBWINDOW_H
