// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "materialwidget.h"

#include <qcombobox.h>
#include <qgridlayout.h>

#include "src/dialog/materials/materials.hpp"
#include "src/dialog/shapes/shape.hpp"
#include "src/dialog/widget/toolbox.h"
#include "src/dialog/widget/widget.h"
#include "src/functions/connectverifier.hpp"

MaterialWidget::MaterialWidget( QWidget* parent, Shape* shape )
	: QWidget{ parent }
	, m_shape{ shape }
	, m_dimensionWiget{ shape->sceneInfo()->widget() }
	, m_materialCombo{ new QComboBox{ this } }
	, m_toolBox{ new ToolBox{ this } }
{
	QGridLayout* grid{ new QGridLayout{ this } };
	setLayout( grid );

	grid->addWidget( m_toolBox, 0, 0 );

	setupWidgets();
	setupConnect();

	std::unique_ptr<Materials> materials{ std::make_unique<Materials>() };
	MaterialsDescription*	   desc{ materials->description() };

	for ( auto& [materialName, materialDensity] : desc->materialsTable() )
	{
		m_materialCombo->addItem( materialName );
		int index{ m_materialCombo->findText( materialName ) };

		const QString toolTip{ materialName + QString::number( materialDensity )
				       + QStringLiteral( " kg/m3" ) };

		m_materialCombo->setItemData( index, toolTip, Qt::ToolTipRole );
	}
}

MaterialWidget::~MaterialWidget() = default;

void MaterialWidget::materialChangedSlot( const QString& material ) const
{
	const int currentIndex{ m_materialCombo->currentIndex() };

	// Enable the widgets if a material is selected.
	if ( Q_LIKELY( currentIndex >= 0 ) )
	{
		for ( uint i{ 0 }; i < m_materialCombo->count(); i++ )
		{
			m_toolBox->widget( i )->setEnabled( true );
		}
	}
}

void MaterialWidget::setupConnect()
{
	ConnectVerifier v;

	v = connect(
		m_materialCombo,
		qOverload<int>( &QComboBox::currentIndexChanged ),
		this,
		[this]() -> void {
			const QString m{ m_materialCombo->currentText() };

			emit materialChanged( m );
		},
		Qt::UniqueConnection );

	v = connect( this,
		     &MaterialWidget::materialChanged,
		     this,
		     &MaterialWidget::materialChangedSlot,
		     Qt::UniqueConnection );
}

void MaterialWidget::setupWidgets()
{
	// Add any widget here
	m_materialCombo->setCurrentIndex( -1 );
	m_materialCombo->setPlaceholderText(
		QStringLiteral( "-- Choose a material to work with: --" ) );

	m_dimensionWiget->setParent( this );
	m_dimensionWiget->setEnabled( false );

	m_toolBox->addItem( m_materialCombo, QStringLiteral( "Materials" ) );
	m_toolBox->addItem( m_dimensionWiget, QStringLiteral( "Dimensions" ) );

	/*
	 * WARNING
	 * Run this last and only keep enabled the combobox widget.
	 * First, we have to find it's index in the combobox.
	 */
	int indexOfCombo{ m_toolBox->indexOf( m_materialCombo ) };

	for ( uint i = 0; i < m_toolBox->count(); i++ )
	{
		if ( i != indexOfCombo ) { m_toolBox->widget( i )->setEnabled( false ); }
		else { continue; }
	}
}
