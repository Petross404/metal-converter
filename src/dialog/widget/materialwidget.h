// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef MATERIALWIDGET_H
#define MATERIALWIDGET_H

#include <qwidget.h>

class Shape;

class Widget;
class QComboBox;
class ToolBox;

/*!
 * \brief Widget to read informations about the materials.
 *
 * This widget provides to the user the interface to work with materials
 * therefore calculate the weight, or the cost of a `Shape` later.
 */
class MaterialWidget: public QWidget
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( MaterialWidget )

public:
	/*!
	 * Constructor
	 *
	 * The constructor discovers the required json file that all the
	 * materials are coded into at runtime.
	 * \param parent is the parent of this widget
	 * \param shape is the shape that is shown on the view.
	 */
	explicit MaterialWidget( QWidget* parent, Shape* shape );
	~MaterialWidget() override;

public slots:
	//! Handle in here the change in the selected material.
	void materialChangedSlot( const QString& material ) const;

signals:
	//! The user selected a different material from the UI.
	void materialChanged( const QString& material ) const;

private:
	Shape* m_shape;

	// Ui elements
	Widget*	   m_dimensionWiget;
	QComboBox* m_materialCombo;
	ToolBox*   m_toolBox;

	void setupConnect();
	void setupWidgets();
};

#endif	  // MATERIALWIDGET_H
