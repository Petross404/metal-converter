// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <qgraphicsview.h>    // for QGraphicsView
#include <qobjectdefs.h>      // for Q_OBJECT
#include <qstring.h>	      // for QString

#include <memory>    // for unique_ptr
#include <optional>

class PushButton;    // lines 18-18
class QObject;
class QWidget;	     // lines 17-17
struct SceneInfo;    // lines 13-13

class ShapeView: public QGraphicsView
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( ShapeView )

public:
	ShapeView( const PushButton* pBtn, std::optional<QWidget*> parent );
	~ShapeView() override;

private:
	std::unique_ptr<SceneInfo> sceneInfo;
};

#endif	  // GRAPHICSVIEW_H
