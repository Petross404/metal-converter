// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "shapeview.h"

#include <qevent.h>
#include <qgraphicsitem.h>
#include <qpainter.h>
#include <qtimer.h>

#include <string>    // for string

#include "shapescene.h"			    // for ShapeScene
#include "src/PushButton/pushbutton.hpp"    // for PushButton
#include "src/dialog/shapes/shape.hpp"	    // for SceneInfo, Shape

ShapeView::ShapeView( const PushButton* pBtn, std::optional<QWidget*> parent )
	: QGraphicsView{ pBtn->shapeType()->sceneInfo()->scene(), parent.value_or( nullptr ) }
	, sceneInfo{ std::make_unique<SceneInfo>( pBtn->shapeType()->sceneInfo() ) }
{
	setWindowTitle( tr( pBtn->shapeType()->name().c_str() ) );
	setMinimumSize( QSize{ 520, 500 } );
}

ShapeView::~ShapeView() = default;
