/*
 * <one line to give the program's name and a brief idea of
 * what it does.> Copyright (C) 2020  Πέτρος Σιλιγκούνας
 * <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef METAL_CALCULATOR_H
#define METAL_CALCULATOR_H

#include <qlist.h>	    // for QList
#include <qmainwindow.h>    // for QMainWindow
#include <qobjectdefs.h>    // for Q_OBJECT, slots
#include <qstring.h>	    // for QString
class QButtonGroup;	    // lines 38-38
class PushButton;	    // lines 39-39
class QAction;		    // lines 40-40
class QMenu;		    // lines 43-43
class QMenuBar;		    // lines 44-44
class QObject;		    // lines 41-41
class QWidget;		    // lines 42-42
namespace Ui {
class CalculatorWindow;
}    // namespace Ui

//! \class CalculatorWindow class
/*!
 * `CalculatorWindow` inherits from `QMainWindow` and it is the main
 * window of this project
 */
class CalculatorWindow: public QMainWindow
{
	Q_OBJECT
	Q_DISABLE_COPY_MOVE( CalculatorWindow )

public:
	//! Constructor of `CalculatorWindow` class
	/*!
	 * `CalculatorWindow` accepts another `QWidget*` as an argument
	 * \param parent The QWidget that will become ConvertDialog's parent
	 */
	explicit CalculatorWindow( QWidget* parent = nullptr );

	//! Virtual default destructor so override
	~CalculatorWindow() override;	 // = default;

private:
	std::unique_ptr<Ui::CalculatorWindow> m_ui; /*!< CalculatorWindow's UI */

	PushButton* m_btnPipe; /*!< `PushButton` of type `Pipe*`*/
	PushButton* m_btnTBar; /*!< `PushButton` of type `TBar*`*/
	PushButton* m_btnHexa; /*!< `PushButton` of type `Hex*`*/
	PushButton* m_btnRoun; /*!< `PushButton` of type `Round*`*/
	PushButton* m_btnAngl; /*!< `PushButton` of type `Angle*`*/
	PushButton* m_btnFlat; /*!< `PushButton` of type `Flat*`*/
	PushButton* m_btnShee; /*!< `PushButton` of type `Sheet*`*/
	PushButton* m_btnCCha; /*!< `PushButton` of type `CChannel*`*/
	PushButton* m_btnSqua; /*!< `PushButton` of type `Square*`*/
	PushButton* m_btnBeam; /*!< `PushButton` of type `Beam*`*/

	QButtonGroup* m_groupBtn;

	QMenuBar* m_menuBar;	    /*!< MenuBar */
	QMenu*	  m_fileMenu;	    /*!< File menu */
	QMenu*	  m_settMenu;	    /*!< Settings menu */
	QMenu*	  m_helpMenu;	    /*!< Help menu */
	QAction*  m_exitAction;	    /*!< Exit action */
	QAction*  m_helpCalculator; /*!< Action to open About Calculator dialog */
	QAction*  m_helpQt;	    /*!< Action to open About Qt dialog */

	/*!
	 * This will create QActions for various actions like:
	 * quitting, opening dialogs and settings etc.
	 */
	void createActions();

	/*!
	 * This will push every `PushButton` of the form inside a `QList`
	 */
	void pushBackToQList();

private slots:
	/*!
	 * Count how many ConvertDialogs are open and emit back to them
	 * in order to update the counters inside these ConvertDialogs.
	 */
	[[nodiscard]] int findConverterDialogs() const;

	/*!
	 * Inside this slot, a `ConvertDialog` is created and shown.
	 * \param pbtn Pass in the slot the pbtn that ignited the connected
	 * signal.
	 * \return void
	 */
	void showDialog( PushButton* pbtn );

	/*!
	 * Show information about this project.
	 */
	void showAbout() const;
};
#endif /* // METAL_CALCULATOR_H */
