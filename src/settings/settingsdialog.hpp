// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 Πέτρος Σιλιγκούνας <petross404@gmail.com>
// SPDX-License-Identifier: GPL-3.0-or-later

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <qdialog.h>

/**
 * @todo write docs
 */
class SettingsDialog: public QDialog
{
	Q_OBJECT

public:
	/**
	 * Default constructor
	 */
	SettingsDialog( QWidget* parent );

	~SettingsDialog() override;
};

#endif	  // SETTINGSDIALOG_H
