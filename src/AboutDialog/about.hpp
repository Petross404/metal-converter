/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2020  Πέτρος Σιλιγκούνας <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ABOUT_H
#define ABOUT_H

#include <qdialog.h>	    // for QDialog
#include <qobjectdefs.h>    // for Q_OBJECT
#include <qstring.h>	    // for QString

#include <memory>

class QObject;
class QWidget;
namespace Ui {
class About;
}    // namespace Ui

//! `About` dialog for
/*! `About` has subclassed `QDialog` in order to show a window with information
 * about
 */
class About: public QDialog
{
	Q_OBJECT

public:
	/**
	 * Constructor
	 */
	explicit About( QWidget* parent );
	explicit About( const About& other )   = delete;
	About& operator=( const About& other ) = delete;
	explicit About( About&& other )	       = delete;
	About& operator=( About&& other )      = delete;

	~About() override /* = default; */;

private:
	std::unique_ptr<Ui::About> m_ui;
};

#endif	  // ABOUT_H
