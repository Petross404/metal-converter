/*!
 * /mainpage Metal Calculator
 * This software calculates the weight or the cost of
 * various shapes of metals.
 *
 * \file main.cpp
 * \author Siligkounas Petros
 */

#include "calculatorwindow.hpp"
#include "qapplication.h"

int main( int argc, char** argv )
{
	QApplication app{ argc, argv };

	CalculatorWindow w;
	w.show();

	return app.exec();
}
