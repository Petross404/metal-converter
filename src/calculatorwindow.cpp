/*
 * <one line to give the program's name and a brief idea of
 * what it does.> Copyright (C) 2020  Πέτρος Σιλιγκούνας
 * <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "calculatorwindow.hpp"

#include <qabstractbutton.h>	// for QAbstractButton
#include <qaction.h>		// for QAction, QAction::MenuRole
#include <qapplication.h>	// for QApplication
#include <qbuttongroup.h>
#include <qeventloop.h>	      // for QEventLoop
#include <qglobal.h>	      // for QFlags, qSwap
#include <qgridlayout.h>      // for QGridLayout
#include <qlist.h>	      // for QList, QList<>::iterator
#include <qmdiarea.h>	      // for QMdiArea
#include <qmdisubwindow.h>    // for QMdiSubWindow
#include <qmenu.h>	      // for QMenu
#include <qmenubar.h>	      // for QMenuBar
#include <qmessagebox.h>      // for QMessageBox, QMessageBox::S...
#include <qnamespace.h>	      // for operator|, QueuedConnection
#include <qobject.h>	      // for qobject_cast
#include <qpushbutton.h>      // for QPushButton
#include <stddef.h>	      // for size_t

#include <string>	  // for string
#include <type_traits>	  // for enable_if<>::type
#include <utility>	  // for as_const, move

#include "AboutDialog/about.hpp"	// for About
#include "PushButton/pushbutton.hpp"	// for PushButton
#include "dialog/convertdialog.hpp"	// for ConvertDialog
#include "dialog/shapes/angle.hpp"	// for Angle
#include "dialog/shapes/flat.hpp"	// for Flat
#include "dialog/shapes/hex.hpp"	// for Hex
#include "dialog/shapes/pipe.hpp"	// for Pipe
#include "dialog/shapes/round.hpp"	// for Round
#include "dialog/shapes/shape.hpp"	// for Shape
#include "dialog/shapes/sheet.hpp"	// for Sheet
#include "dialog/shapes/t_bar.hpp"	// for TBar
#include "dialog/widget/mdisubwindow.h"
#include "functions/connectverifier.hpp"    // for ConnectVerifier
#include "ui_calculatorwindow.h"	    // for CalculatorWindow

//! Alias for a very big name
using Button = QMessageBox::StandardButton;

CalculatorWindow::CalculatorWindow( QWidget* parent )
	: QMainWindow{ parent }
	, m_ui{ std::make_unique<Ui::CalculatorWindow>() }
	, m_btnPipe{ new PushButton{ this, new Pipe{ this } } }
	, m_btnTBar{ new PushButton{ this, new TBar{ this } } }
	, m_btnHexa{ new PushButton{ this, new Hex{ this } } }
	, m_btnRoun{ new PushButton{ this, new Round{ this } } }
	, m_btnAngl{ new PushButton{ this, new Angle{ this } } }
	, m_btnFlat{ new PushButton{ this, new Flat{ this } } }
	, m_btnShee{ new PushButton{ this, new Sheet{ this } } }
	, m_groupBtn{ new QButtonGroup{ this } }
{
	createActions();
	m_ui->setupUi( this );

	setSizePolicy( QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Expanding );

	pushBackToQList();

	ConnectVerifier v;

	v = connect( m_ui->quit,
		     &QPushButton::clicked,
		     m_exitAction,
		     &QAction::triggered,
		     Qt::ConnectionType::UniqueConnection );
}

CalculatorWindow::~CalculatorWindow() = default;

void CalculatorWindow::createActions()
{
	// Create a menubar
	m_menuBar = new QMenuBar{ this };

	/*
	 * Create a File menu to hold the following actions:
	 *
	 * 1) Exit
	 */
	m_fileMenu = menuBar()->addMenu( tr( "File" ) );

	m_exitAction = new QAction{ tr( "Exit" ), this };
	m_exitAction->setStatusTip( tr( "Exit the program" ) );
	m_exitAction->setMenuRole( QAction::MenuRole::QuitRole );
	m_fileMenu->addAction( m_exitAction );

	connect( m_exitAction,
		 &QAction::triggered,
		 this,
		 &QApplication::quit,
		 Qt::ConnectionType::QueuedConnection );

	/*
	 * Create an empty menu for now
	 */
	m_settMenu = menuBar()->addMenu( tr( "Settings" ) );

	/*
	 * Create a Help menu to hold these actions:
	 *
	 * 1) About Calculator
	 * 2) About Qt
	 */
	m_helpMenu = menuBar()->addMenu( tr( "Help" ) );

	m_helpCalculator = new QAction{ tr( "About Calculator" ), this };
	m_helpCalculator->setStatusTip( tr( "Inormation about Calculator" ) );
	m_helpCalculator->setMenuRole( QAction::MenuRole::AboutRole );
	m_helpMenu->addAction( m_helpCalculator );

	connect( m_helpCalculator,
		 &QAction::triggered,
		 this,
		 &CalculatorWindow::showAbout,
		 Qt::ConnectionType::UniqueConnection );

	m_helpQt = new QAction{ tr( "About Qt" ), this };
	m_helpQt->setStatusTip( tr( "Information about Qt" ) );
	m_helpQt->setMenuRole( QAction::MenuRole::AboutQtRole );
	m_helpMenu->addAction( m_helpQt );

	connect( m_helpQt,
		 &QAction::triggered,
		 this,
		 &QApplication::aboutQt,
		 Qt::ConnectionType::UniqueConnection );
}

void CalculatorWindow::pushBackToQList()
{
	QList<PushButton*> list{ findChildren<PushButton*>() };

	for ( size_t i{ 0 }; i < list.count(); i++ )
	{
		PushButton* pbtn{ list.at( i ) };

		/*
		 * (Q)ButtonGroup can hold only QAbstractButtons and not
		 * (Q)PushButtons, so there are some casts in order to:
		 *
		 * 1) Add buttons to btnGroup
		 *
		 * 2) Add PushButton widget to layout and cast each (abstract)
		 * button back to PushButton so we can connect it's signal
		 * with the one from ButtonGroup
		 */

		auto* constAbstBtn{ qobject_cast<QAbstractButton*>( std::as_const( pbtn ) ) };
		auto* abstBtn{ const_cast<QAbstractButton*>( constAbstBtn ) };
		m_groupBtn->addButton( abstBtn, i );

		// Cast the abstract buttons to PushButton and set their parent
		pbtn = qobject_cast<PushButton*>( abstBtn );
		pbtn->setParent( m_ui->centralWidget );

		// Place the buttons on the layout
		m_ui->gridLayout_buttons->addWidget( pbtn );

		// Connect button to showDialog() when clicked.
		ConnectVerifier v;
		v = connect( pbtn,
			     &PushButton::pbtnClicked,
			     this,
			     &CalculatorWindow::showDialog,
			     Qt::UniqueConnection );
	}
}

int CalculatorWindow::findConverterDialogs() const
{
	/*
	 * Count how many ConvertDialogs are open and emit back
	 * to update the counters inside ConvertDialogs.
	 */
	int counter{ findChildren<ConvertDialog*>().count() };

	return counter;
}

void CalculatorWindow::showDialog( PushButton* pbtn )
{
	/*
	 * Get the Shape's (std::string) name and make it QString
	 */
	QString name{ QString::fromUtf8( pbtn->shapeType()->name().c_str() ) };

	ConvertDialog* convertdlg{ new ConvertDialog{ pbtn, this, name } };
	MdiSubWindow*  subWindow{ new MdiSubWindow{ m_ui->mdiArea, std::nullopt } };
	subWindow->setWidget( convertdlg );

	m_ui->mdiArea->addSubWindow( subWindow, Qt::SubWindow );

	ConnectVerifier v;

	// Update window title
	v = connect(
		subWindow,
		&QMdiSubWindow::aboutToActivate,
		this,
		[this, subWindow]() -> void { setWindowTitle( subWindow->windowTitle() ); },
		Qt::UniqueConnection );

	v = connect( convertdlg,
		     &ConvertDialog::aboutToCloseDlg,
		     subWindow,
		     &QMdiSubWindow::close,
		     Qt::UniqueConnection );

	/*
	 * The only way to avoid reaching the closing bracket ( } ) of this slot
	 * and deallocating the mentioned pointer, is to use a local QEventLoop.
	 */
	QEventLoop localEventLoop;

	v = connect( convertdlg,
		     &ConvertDialog::aboutToCloseDlg,
		     &localEventLoop,
		     &QEventLoop::quit,
		     Qt::QueuedConnection );

	convertdlg->show();
	localEventLoop.exec();
}

void CalculatorWindow::showAbout() const
{
	About* about{ new About( const_cast<CalculatorWindow*>( this ) ) };
	about->setWindowFlags( Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::Dialog );
	about->exec();
}
