/*
 * <one line to give the program's name and a brief idea of
 * what it does.> Copyright (C) 2020  Πέτρος Σιλιγκούνας
 * <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef PUSHBUTTON_H
#define PUSHBUTTON_H

#include <qobjectdefs.h>    // for Q_OBJECT, signals
#include <qpushbutton.h>    // for QPushButton
#include <qstring.h>	    // for QString
class QObject;		    // lines 29-29
class QWidget;		    // lines 30-30
class Shape;		    // lines 31-31

/*!
 * \brief `PushButton` is a subclassed `QPushButton`.
 *
 * `PushButton` is a class that inherits `QPushButton` and has a `Shape**` member too.
 * The later can be `Angle**`, `TBar**`, `Pipe**`, `Sheet**` etc. It's basic purpose is to
 * give to the `QPushButton` type some extra functionality. We need to open `ConvertDialog`
 * with a `QPushButton` but at the same time we need the former to inherit some functions
 * and properties based on the `PushButton` that the user clicked. \sa `Shape`
 */
class PushButton: public QPushButton
{
	Q_OBJECT
public:
	/*!
	 * Default constructor
	 */
	PushButton() = delete;

	/*!
	 *  `PushButton` Constructor
	 * \param btn is the `QPushButton*` that will initiliaze this class
	 * \param shape is the `Shape*` that will initiliaze the m_shape member
	 */
	explicit PushButton( QWidget* parent, Shape* shape );

	//! Deleted copy constructor of `PushButton`
	explicit PushButton( const PushButton& pbtn ) = delete;
	//! Deleted copy assignement operator of `PushButton`
	PushButton& operator=( const PushButton& pbtn ) = delete;
	//! Deleted move constructor of `PushButton`
	explicit PushButton( PushButton&& pbtn ) noexcept = delete;
	//! Deleted move assignement operator of `PushButton`
	PushButton& operator=( PushButton&& pbtn ) noexcept = delete;

	//! Virtual defaulted dtor so override
	~PushButton() override;	   // = default;

	/*!
	 * \brief Access the `Shape` type of the `PushButton`
	 *
	 * Eash `PushButton` has a private `Shape**` member that is initiliazed
	 * during the construction of the object. It's purpose is for every
	 * `PushButton` object to differentiate from the others, based on the
	 * `Shape` we want it to draw in `ConvertDialog` window.
	 * \return The exact derived type of `Shape*` ie the m_shape member
	 */
	[[nodiscard]] Shape* shapeType() const;

signals:
	//! Custom signal to emit the clicked PushButton into slots
	/*!
	 * The classic `QPushButton::clicked(bool)` is not sufficient in this case.
	 * We need a signal that will cary the `PushButton` that triggered it.
	 * \param pbtn is the clicked pbtn that triggered this.
	 */
	void pbtnClicked( PushButton* pbtn );

private:
	/*
	 * If this type was Shape* and not Shape**, access to derived
	 * classes of Shape wouldn't be possible. This means that virtual
	 * functions of `Shape` wouldn't work based on inheritance.
	 */
	Shape* m_shape;
};

#endif	  // PUSHBUTTON_H
