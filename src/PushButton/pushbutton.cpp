/*
 * <one line to give the program's name and a brief idea of
 * what it does.> Copyright (C) 2020  Πέτρος Σιλιγκούνας
 * <petross404@gmail.com>
 *
 * This program is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "pushbutton.hpp"

#include <qicon.h>	   // for QIcon
#include <qnamespace.h>	   // for QueuedConnection
#include <qpixmap.h>	   // for QPixmap
#include <qsize.h>	   // for QSize

#include <functional>
#include <string>     // for string
#include <utility>    // for move

#include "../dialog/shapes/shape.hpp"	 // for Shape
#include "../functions/connectverifier.hpp"

PushButton::PushButton( QWidget* parent, Shape* shape )
	: QPushButton{ parent }
	, m_shape{ shape }
{
	QString name{ QString{ shapeType()->name().c_str() } };

	QString iconName{ QString{ ":/resources/shapes/%1.png" }.arg( name.toLower() ) };
	QIcon	shapeIcon{ QPixmap{ iconName } };

	setText( name );
	setIcon( shapeIcon );
	setObjectName( QString{ "%1" + name }.arg( QStringLiteral( "m_btn" ) ) );

	// setMinimumSize because the buttons are small
	setMinimumSize( QSize{ 165, 32 } );

	ConnectVerifier v;

	// Connect a custom signal to QPushButton::clicked.
	v = connect( this,
		     &PushButton::clicked,
		     this,
		     std::bind( &PushButton::pbtnClicked, this, this ),
		     Qt::UniqueConnection );
}

PushButton::~PushButton() = default;

Shape* PushButton::shapeType() const { return m_shape; }
