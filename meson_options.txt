option('enable_include_what_you_use',
	type : 'boolean',
	value : false,
	description : 'Use include-what-you-use tool to report superfluous #includes')

option('enable_build_doc',
	type : 'boolean',
	value : false,
	description : 'Build documentation with Doxygen')

