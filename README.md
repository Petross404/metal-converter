//! \mainpage

# Metal Calculator (Work In Progress)

Metal Calculator is a Qt5 application that aims to help engineers calculate the costs of simple parts. It is written
in C++17 and Qt-5.15.
![Screenshot](src/resources/image/logo_200x200.png)

## Dependencies

### Linux

Refer to your distribution's package manager in case Metal Calculator has been packaged for it. Otherwise,
you need to compile and install it manually. First install these dependencies:

Runtime Dependencies
``` bash
	Qt5::Core
	Qt5::Widgets
	Qt5::Gui

	The GL Vendor-Neutral Dispatch library (libglvnd)
	The OpenGL Utility Library (glu)
```

Compile Time Dependencies
``` bash
	${Runtime Dependencies}
	Doxygen (if you need the documentation)
	include-what-you-use (if you need to find superfluous includes)
```

### Windows

Not tested. It should compile and run with a simple Qt5 installation and a C++17 compiler but this `CMakeLists.txt` does not handle the MSVC compiler.


In order to compile Metal Calculator for Linux/Mac you can use CMake or Meson.
``` bash
git https://gitlab.com/Petross404/metal-converter.git
cd metal_converter
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_BUILD_TYPE={Release | Debug}
sudo make install #optional
```

One could run the `script-{cmake|meson}-build.sh` script on the root directory of the project.

Options that control the build:
``` cmake
	-DENABLE_INCLUDE_WHAT_YOU_USE	#It uses include-what-you-use to report superfluous #includes.

	-DBUILD_DOC=ON			#It builds Doxygen documentation.

	-DENABLE_ASAN=OFF 		#For Debug builds. It detects out-of-bounds accesses to heap, stack and globals, invalid free etc

	-DENABLE_LSAN=OFF 		#For Debug builds. It detects memory leaks. It is incuded in ASAN.

	-DENABLE_MSAN=OFF 		#For Debug builds.
```
Inside `build` directory you will find the executable Calculator.

```meson
	-Db_sanitize={none | address | thread | undefined | memory | address,undefined }
	-Db_ndebug={true | false | if-release}	# Disable asserts

	-Denable_build_doc=true			# It builds Doxygen documentation
```
Inside `build/src` directory you will find the executable Calculator.

## Acknowledgments:
- Πέτρος Σιλιγκούνας (Petross404)
https://gitlab.com/Petross404
